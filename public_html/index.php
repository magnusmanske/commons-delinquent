<?php
/*
Delinker Log Interface
by Magnus Manske and Steinsplitter

Debug:
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');
*/

require_once ( '../shared.inc' ) ;

$image = trim ( get_request ( 'image' , '' ) ) ;
$action = get_request ( 'action' , 'any' ) ;
$result = get_request ( 'result' , 'any' ) ;

function esc ( $s ) {
        return str_replace ( '"' , '&quot' , str_replace ( "'" , '&#39;' , $s ) ) ;
}

$status = array (
        0 => "<span tt='pending' style='color:black'>Pending</span>" ,
        1 => "<span tt='done' style='color:green'>Done</span>" ,
        2 => "<span tt='skipped' style='color:red'>Skipped</span>",
        42=>"<span tt='manual' style='color:brown'>Manual review</span>",
        127=> "<span tt='maint' style='color:purple'>Problem</span>",
) ;

$cd = new CommonsDelinquent() ;
$db = $cd->getToolDB() ;
if(isset($_GET["cnt"]) && $_GET["cnt"] == "true"){
$sql = "select count(file) as cnt from  event where done=0" ;
$result = $cd->runQuery ( $db , $sql ) ;
while($o = $result->fetch_object()) $pending = $o->cnt ;
}
print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">
<head>
        <meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />
        <title>Commons Delinker</title>
        <link href=\"//tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css\" rel=\"stylesheet\">
        <script src=\"//tools-static.wmflabs.org/tooltranslate/tt.js\"></script>
        <script src=\"//tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>
        <script src=\"https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js\"></script>
  </style>
<script>
jQuery(document).ready(function(){
    jQuery('#ftm').appendTo('#ftm2')
});

$(document).on('click', '#cdbutton', function() {
  $('#delinkerloader').show();
});

$(document).on('click', '#cdbuttonsub', function() {
  $('#subload').show();
});
</script>
</head>
<body>";
?>
<div class="modal fade" id="ModalReplace" tabindex="-1" aria-labelledby="ModalReplace" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Global Replace</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Commons Delinker can replace files globally (across almost all wikis), for example when a file has been renamed locally.<br>
       <div tt="intro2">To replace files globally, see <a href='https://commons.wikimedia.org/wiki/User:CommonsDelinker/commands'>this page</a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalCode" tabindex="-1" aria-labelledby="ModalCode" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title-2" id="exampleModalLabel2">Commons Delinker Source Code</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      The source code of Commons Delinquent (Delinker v2) is located at Bitbucket. Pull requests are welcome!<br><br>
      Bitbucket repo: <a href='//bitbucket.org/magnusmanske/commons-delinquent/src'>/commons-delinquent/src</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
print "      <div class=\"d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm\">
        <h5 class='my-0 mr-md-auto font-weight-normal'><span tt=\"delinquent\">Commons Delinker Log</span></h5>
         <nav class=\"my-2 my-md-0 mr-md-3\">
          <a class=\"p-2 text-dark\" href=\"#\" data-toggle=\"modal\" data-target=\"#ModalReplace\">Global Replace</a>
          <a class=\"p-2 text-dark\" href=\"//tools.wmflabs.org/commons-delinquent/?image=&action=null&result=null&cnt=true\" id=\"cdbutton\"><span class=\"glyphicon glyphicon-road\"></span> <span tt=\"pendingedits\">pending edits</span></a>
          <a class=\"p-2 text-dark\" href=\"//tools.wmflabs.org/commons-delinquent/?image=&action=null&result=null&status=true\"><span class=\"glyphicon glyphicon-flag\"></span> <span tt=\"bugrep\">Report a Bug</a></span>
          <a class=\"p-2 text-dark\" href=\"#\" data-toggle=\"modal\" data-target=\"#ModalCode\"><span class=\"glyphicon glyphicon-wrench\"></span> <span tt=\"code\">code</span></a>
        </nav>
          <div class=\"nav-item\" style = \"padding-top: 5px;\" id=\"tooltranslate_wrapper\"></div>
    </div>
  </div>
";
require_once ( "/data/project/tooltranslate/public_html/tt.php") ;
$tt = new ToolTranslation ( array ( 'tool' => 'delinquent' , 'language' => 'en', 'fallback' => 'en', 'highlight_missing' => false ) ) ;
print $tt->getJS('#tooltranslate_wrapper') ;
print $tt->getJS() ;
print "<div class=\"container\">
";
print "<a href='//commons.wikimedia.org/wiki/File:CommonsDelinker.svg' class='image' style = 'float:right;'><img alt='CommonsDelinker.svg' src='//upload.wikimedia.org/wikipedia/commons/thumb/d/da/CommonsDelinker.svg/40px-CommonsDelinker.svg.png' srcset='//upload.wikimedia.org/wikipedia/commons/thumb/d/da/CommonsDelinker.svg/60px-CommonsDelinker.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/d/da/CommonsDelinker.svg/80px-CommonsDelinker.svg.png 2x' data-file-width='300' data-file-height='400' height='53' width='40'></a>";
print "<div class='lead'>
<span tt=\"intro1\">Commons Delinker <small>(Commons Delinquent <span class='badge badge-secondary'>v2</span>)</small> finds files that were deleted on Commons, and removed their entries on other wikis to avoid ugly media redlinks.</span>
<div id='delinkerloader' style='display:none'><center>
<div class='d-flex justify-content-center'>
  <div class='spinner-border' role='status'>
  </div>
</div>
<b>Loading...</b></center></div>
";

if(isset($_GET["cnt"]) && $_GET["cnt"] == "true"){
print "<br><br><p class='alert alert-info'><span tt=\"cpe\">Currenty pending edits:</span> $pending</p>";
print "<a class='btn btn-secondary' href='index.php' role='button'><span tt=\"home\">Home</span></a>";
exit();
}

if(isset($_GET["status"]) && $_GET["status"] == "true"){
/*
Won't work with kubernetes webservice:

$output2 = shell_exec("job -v demon");
$output3 = str_replace("'demon'","",$output2);
if (preg_match('/has been running/', $output2)) {
    print "<br><br><p class='alert alert-success'><big><span tt=\"botrunning\">Bot is running...</span></big><br><small>$output3</small></p>";
} else {
    print "<br><br><p class='alert alert-danger'><big><span tt=\"botnotrunning\">Bot is not running.</span></big><br>$output3</p>";
}
*/
//print "<a class='btn btn-default' href='index.php' role='button'><span tt=\"home\">Home</span></a>&nbsp;";
print "<br><br><a class='btn btn-secondary' href='https://bitbucket.org/magnusmanske/commons-delinquent/issues?status=new&status=open' role='button'>Bugtracker</a>&nbsp;";
print "<a class='btn btn-primary' href='https://bitbucket.org/magnusmanske/commons-delinquent/issues/new' role='button'>Report a Bug</a>&nbsp;";
print "</div></div>";
exit();
}

print "
</div>

<div><form method='get'>
<br>
<h3><span tt='search'>Search</span></h3>
<table class='table'>
<tbody>
<tr><th><span tt=\"fn\">File name</span></th><td>
<div class='input-group mb-3'>
  <div class='input-group-prepend'>
    <span class='input-group-text' id='basic-addon3'>File:</span>
  </div>
  <input type='text' name='image' class='form-control' id='basic-url' aria-describedby='basic-addon3' />
</div>

</div>
</div>
</td></tr>
<tr><th><span tt=\"action\">Action</span></th><td><select class='form-control' style='width:auto' name='action'>
<option tt=\"any\" value='any' " . ($action=='any'?'selected':'') . ">Any</option>
<option tt=\"unlink\" value='unlink' " . ($action=='unlink'?'selected':'') . ">Unlink</option>
<option tt=\"replace\" value='replace' " . ($action=='replace'?'selected':'') . ">Replace</option>
</select></td></tr>
<tr><th>Result</th><td><select class='form-control' style='width:auto' name='result'>
<option tt=\"any\" value='any' " . ($result=='any'?'selected':'') . ">Any</option>
<option tt=\"pending\" value='0' " . ($result=='0'?'selected':'') . ">Pending</option>
<option tt=\"done\" value='1' " . ($result=='1'?'selected':'') . ">Done</option>
<option tt=\"skipped\" value='2' " . ($result=='2'?'selected':'') . ">Skipped</option>
</select></td></tr>
<tr><th></th><td><input type='submit' id='cdbutton' value='Filter' class='btn btn-primary' /> <a href='?'  class='btn btn-secondary'><span tt=\"reset\">Reset form</span></a></td></tr>
</tbody>
</div>
</table>
</form></div><br>
";

if(isset($_GET['image'])){
   print "<h3><span tt=\"results-for\">Results for File:</span>" . htmlspecialchars($image) . " <a title='External Link' href='https://commons.wikimedia.org/w/index.php?title=File:" . urlencode($image) . "' target='_blank' rel='noopener'><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Icon_External_Link.svg/20px-Icon_External_Link.svg.png' alt='External Link' width='23' height='23' /></a></h3>" ;

} else {
    print "<h3><span tt=\"raac\">Recent actions</span></h3>";
}

$mode = get_request ( 'mode' , 'latest' ) ;


if ( $mode == 'latest' ) {
        $num = get_request ( 'num' , 100 ) * 1 ;
        $offset = get_request ( 'offset' , 0 ) * 1 ;

        $where = array() ;
        if ( $image != '' ) $where[] = "file='" . $cd->getDBsafe(ucfirst(str_replace(' ','_',$image))) . "'" ;
        if ( $action != 'any' ) $where[] = "action='" . $cd->getDBsafe($action) . "'" ;
        if ( $result != 'any' ) $where[] = "done='" . $cd->getDBsafe($result*1) . "'" ;

        $sql = "SELECT * FROM event" ;
        if ( count($where) > 0 ) $sql .= " WHERE " . implode(" AND ",$where) ;
        $sql .= " ORDER BY timestamp DESC,log_timestamp DESC limit $num offset $offset" ;
//      print "<pre>$sql</pre>" ;

       $osnum="";
       if(isset($_GET['offset']) && $_GET['offset'] != "0") {
         $paginst = "notdisabled";
         $osnum = str_replace("-100", "0", $offset-$num);
       } else {
         $paginst = "disabled";
       }
       $imgx="";
       if(isset($_GET['image'])) {
        $imgx = "&image=" . urlencode($_GET['image']);
       }
        print "<div id='ftm'><center><nav aria-label='Nav'>
  <ul class='pagination justify-content-center'>
    <li class='page-item ". $paginst ."'>
      <a class='page-link' href='?action=$action&result=$result" . $imgx . "&mode=$mode&num=$num&offset=" . ($osnum) . "'>Newer $num</a>
    </li>
    <li class='page-item'>
      <a class='page-link' href='?action=$action&result=$result" . $imgx . "&mode=$mode&num=$num&offset=" . ($offset+$num) . "'>Older $num</a>
    </li>
  </ul>
</nav>
</center>
</div>
" ;


        $result = $cd->runQuery ( $db , $sql ) ;
        print "<table class='table table-condensed table-striped'>" ;
        print "<thead><tr class='bg-secondary text-white'><th><span tt=\"time\">Time</span></th><th><span tt=\"file\">File</span></th><th><span tt=\"page\">Page</span></th><th><span tt=\"status\">Status</span></th></tr></thead>" ;
        print "<tbody style='font-size:9pt'>" ;
        $resultsr = "no";
        while($o = $result->fetch_object()) {
                $resultsr = "yes";
                print "<tr>" ;
                print "<td nowrap>" . substr($o->timestamp,0,4).'-'.substr($o->timestamp,4,2).'-'.substr($o->timestamp,6,2).'&nbsp;'.substr($o->timestamp,8,2).':'.substr($o->timestamp,10,2).':'.substr($o->timestamp,12,2) ;
                if ( $o->action == 'replace' ) print "<br/><span class='badge badge-secondary'> <span tt=\"rep\">Replacing file</span></span>" ;
                print "</td>" ;

                if ( $o->action == 'replace' ) {
                        print "<td><a target='_blank' href='//commons.wikimedia.org/wiki/File:" . htmlspecialchars($o->file) . "'>" . str_replace('_',' ',$o->file) . "</a>" ;
                        print "<br/>&Rarr;<a target='_blank' href='//commons.wikimedia.org/wiki/File:" . htmlspecialchars($o->replace_with_file) . "'>" . str_replace('_',' ',$o->replace_with_file) . "</a></td>" ;
                } else {
                        print "<td><a target='_blank' href='//commons.wikimedia.org/wiki/Special:Log?page=File:" . htmlspecialchars($o->file) . "'>" . str_replace('_',' ',$o->file) . "</a></td>" ;
                }

                print "<td><a target='_blank' title='Bot edits' href='//" . $cd->wiki2server($o->wiki) . "/wiki/Special:Contributions/" . urlencode($cd->config['name']) . "'>" . $o->wiki . "</a>:" ;
                print "<a target='_blank' href='//" . $cd->wiki2server($o->wiki) . "/wiki/" . urlencode($o->page) . "'>" . str_replace('_',' ',$o->page) . "</a></td>" ;

                print "<td style='width:120px'>" . $status[$o->done] ;
                if ( $o->note != '' ) print "<br/><small>" . $o->note . "</small>" ;
                print "</td>" ;
                print "</tr>" ;
        }

        print "</tbody></table>" ;

       if ( $resultsr == "no" ) print "<div class='alert alert-danger' role='alert'><span tt='noresult'>No results</span></div>";

       print "<div><div id='ftm2'></div>" ;


}
?>
<div class='alert alert-secondary' role='alert'>
  <h4 class='alert-heading'><div tt="odl">Other Bots</div></h4>
  <p><div tt="odl-desc">This bot (CommonsDelinker) runs across all Wikimedia Wikis, but on some wikis a additional bot is removing red links. Below you can find the list of additional bots, you may want to check those logs as well.</div></p>
  <hr>
 <div class='justify-content-center'>
  <p><b><span tt="bots1">Bots:</span></b> Dateientlinkerbot | Filedelinkerbot</p>
<center>
<div id="subload" style='display:none'>
<div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<br><br>
</div>
<div id="xdelink"><a href="/x-delinkers.php?file=<?php if( isset( $_GET["image"] ) )  echo urlencode($_GET["image"]); ?>"  id="cdbuttonsub" class='btn btn-info'>View logs</a></div>
</center>
</div>
</div>
<div id="lwindow"></div>
</div>
<?php
print get_common_footer() ;

?>
